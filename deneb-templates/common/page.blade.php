<!DOCTYPE html>

<html lang="{{ $site->general->lang }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#4a86cf">
	<meta name="application-name" content="GNOME Pastebin">
	<link rel="icon" type="image/png" href="https://static.gnome.org/img/gnome-icon.svg" />
	<title>{{ $site->general->title }}</title>

	<link rel="alternate" type="application/rss+xml" title="RSS" href="{{ url('feed') }}" />

	<link href="https://static.gnome.org/img/favicon.ico" rel="shortcut icon" />
	<link href="https://static.gnome.org/css/deneb.min.css" rel="stylesheet" />
	<link href="{{ View::asset('css/stickynotes.css') }}" rel="stylesheet" />

	<script type="text/javascript" src="{{ View::asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ View::asset('js/jquery.cookie.js') }}"></script>
	<script type="text/javascript" src="{{ View::asset('js/jquery.scrollto.js') }}"></script>
	<script type="text/javascript" src="https://static.gnome.org/js/deneb.min.js"></script>
	<script type="text/javascript" src="{{ View::asset('js/stickynotes.js') }}"></script>
	<script type="text/javascript" src="//www.google.com/jsapi"></script>

	<script type="text/javascript">
		var ajaxUrl = "{{ url('ajax') }}";
		var ajaxNav = {{ $site->general->ajaxNav ? 'true' : 'false' }};
	</script>
</head>

<body class="gnome-body" data-toggle="ajax" data-context="{{ $context }}">
	<div class="loader">
		@include('skins.deneb.common.loader')
	</div>

<header class="gnome-header">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="icon-bar top-bar"></span>
					<span class="icon-bar middle-bar"></span>
					<span class="icon-bar bottom-bar"></span>
				</button>

				<a class="gnome-navbar-brand" href="{{ url() }}"><img src="https://static.gnome.org/img/gnome-logo.svg" alt="{{ $site->general->title }}"></a>
			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					{{ View::menu('navigation') }}
				</ul>
			</div>
		</div>
	</nav>
</header>

<div class="gnome-content">
	<div class="container">
		@if ( ! empty($site->general->bannerTop))
			<div class="row">
				<div class="col-sm-12">
					{{ $site->general->bannerTop }}
				</div>
			</div>
		@endif

		@yield('body')

		@if ( ! empty($site->general->bannerBottom))
			<div class="row">
				<div class="col-sm-12">
					{{ $site->general->bannerBottom }}
				</div>
			</div>
		@endif
	</div>
</div>

<div class="footer">
	<div class="container triangles">
		<div class="row">
			<div class="col-xs-12">
				<p>
					&copy; <a href="https://gnome.org"> The GNOME Project</a>
				</p>
				<p>
				{{ $site->general->title }}.
				Hosted by <a href="https://www.redhat.com/" target="_blank">Red Hat</a>.
				Powered by Sticky Notes &copy; 2014 Sayak Banerjee.
				</p>
				<p>{{ $site->general->copyright }}</p>
				@if ($active AND $role->admin)
					<small>{{ sprintf(Lang::get('global.statistics'), microtime(true) - LARAVEL_START, count(DB::getQueryLog()) - 1) }}</small>
				@endif

				@if (Antispam::flags()->php)
					<!-- Honeypot, do not click! -->
					<a href="http://www.ssdfreaks.com/household.php?to=857"><!-- agreement --></a>
				@endif
			</div>
		</div>
	</div>
</div>
</body>

</html>
